package uva.ada.samalfa_danbarb.distanciaedicion;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.ArrayList;

import uva.ada.samalfa_danbarb.distanciaedicion.Solucion.Operacion;
import uva.ada.samalfa_danbarb.distanciaedicion.Solucion.Paso;

/**
 * Práctica 2 : Análisis y Diseño de Algoritmos - Curso 2014/2015
 * 
 * Desarrollo de un algoritmo para la obtención de la Distancia Mínima de 
 * Edición entre 2 cadenas de caractéres mediante técnicas de programación 
 * dinámica. Obtención de todas las posibles secuencias de operaciones entre
 * los 2 textos que comparten esta distancia mínima.
 * 
 * @author Samuel Alfageme Sainz (samalfa)
 * @author Daniel Barba Gutiérrez(danbarb)
 */
public class DistanciaEdicion {
	private static String parte1 = "p2_samalfa_danbarb_parte1.txt";
	private static String parte2 = "p2_samalfa_danbarb_parte2.txt";
	
	public static void main(String[] args){
		vaciaFichero(parte1);
		vaciaFichero(parte2);
		
		String[] parejas = leeFichero("entrada.txt");
		for (int i = 0; i<parejas.length; i+=2){
			distanciaMinima(parejas[i], parejas[i+1]);
			secuenciasPosibles(parejas[i], parejas[i+1]);
		}
	}
	
	/**
	 * Calcula la distancia de edicion para dos cadenas dadas, creando un 
	 * objeto Solucion que encapsula los datos de la secuencia de pasos a 
	 * seguir y el valor de la distancia.
	 * 
	 * @param s1 - Texto a obtener
	 * @param s2 - Texto de partida
	 */
	public static void distanciaMinima(String s1, String s2){
		// Normalización de los strings: se considera que 'a' y 'A' son iguales
		s1 = s1.toUpperCase();
		s2 = s2.toUpperCase();
		
		int[][] matrizEdicion = calculaMatrizEdicion(s1, s2);
		int adjI,adjA,adjD;
		Solucion solucion = new Solucion();
				
		int actual;
		int i = s1.length();
		int j = s2.length();
		
		while(i!=0 && j!=0){
			adjI = matrizEdicion[i][j-1];
			adjA = matrizEdicion[i-1][j];
			adjD = matrizEdicion[i-1][j-1];
			actual = matrizEdicion[i][j];
			if(adjD<=Math.min(adjI, adjA) && ((adjD==actual)||(adjD==actual-1))){
				if(adjD==actual-1)
					solucion.addPaso(new Paso(Operacion.SUSTITUIR,s1.charAt(i-1),s2.charAt(j-1)));
				else
					solucion.addPaso(new Paso(Operacion.COPIAR, s1.charAt(i-1)));
				i--;
				j--;
			}else if(adjI<=adjA && ((adjI==actual)||(adjI==actual-1))){
				solucion.addPaso(new Paso(Operacion.INSERTAR, s2.charAt(j-1)));
				j--;
			}else{
				solucion.addPaso(new Paso(Operacion.BORRAR, s1.charAt(i-1)));
				i--;
			}
		}
		
		solucion.setDistancia(matrizEdicion[s1.length()][s2.length()]);
		
		//printMatrizEdicion(s1, s2, matrizEdicion);
		imprimeFichero(parte1, s1 + "\n" + s2 + "\n" + "Distancia = " + solucion.getDistancia());
		imprimeFichero(parte1, solucion.toString());
	}
	
	/**
	 * Calcula todas las posibles secuencias que cumplen con la distancia 
	 * minima para dos cadenas dadas.
	 * 
	 * @param s1 - Primera cadena a analizar.
	 * @param s2 - Segunda cadena a analizar
	 * @return un array de Solucion con todas las posibilidades.
	 */
	public static void secuenciasPosibles(String s1, String s2){
		// Normalización de los strings: se considera que 'a' y 'A' son iguales
		s1 = s1.toUpperCase();
		s2 = s2.toUpperCase();
		int[][] matrizEdicion = calculaMatrizEdicion(s1, s2);
		int dMin = matrizEdicion[s1.length()][s2.length()];	
		Solucion solucion = new Solucion();
		int i = s1.length();
		int j = s2.length();
		imprimeFichero(parte2, s1 + "\n" + s2 + "\n" + "Distancia = " + dMin);
		resuelvePosibles(i, j, matrizEdicion, dMin, solucion, s1, s2);
	}
	
	/*
	 * Resuelve recursivamente todas las posibilidades, imprimiendo aquellas que
	 * sean validas y completas
	 */
	private static void resuelvePosibles(int i, int j, int[][] matrizEdicion, int dMin, Solucion solucion, String s1, String s2){
		int[][] posibilidades = {{0,1},{1,0},{1,1}};
		int iNext, jNext;
		for (int[] posib : posibilidades){
			iNext = i - posib[0];
			jNext = j - posib[1];
			if (iNext<0 || jNext<0){
				continue;
			}
			if (i == iNext){
				solucion.addPaso(new Paso(Operacion.INSERTAR, s2.charAt(jNext)));
			} else if (j == jNext){
				solucion.addPaso(new Paso(Operacion.BORRAR, s1.charAt(iNext)));
			} else {
				if (matrizEdicion[i][j]==matrizEdicion[iNext][jNext]){
					solucion.addPaso(new Paso(Operacion.COPIAR, s1.charAt(iNext)));
				} else {
					solucion.addPaso(new Paso(Operacion.SUSTITUIR,s1.charAt(iNext),s2.charAt(jNext)));
				}
			}
			
			if (aceptable(i, j, iNext, jNext, solucion, dMin, matrizEdicion, s1, s2)){
				if (iNext==0 && jNext==0){
					imprimeFichero(parte2, solucion.toString());
				} else if (iNext!=0 || jNext!=0) {
					resuelvePosibles(iNext, jNext, matrizEdicion, dMin, solucion, s1, s2);
				}
			}
			solucion.removePaso();
		}
	}
	
	/*
	 * Encapsula las comprobaciones necesarias para que una solucion parcial sea
	 * correcta y pueda incorporarse a la solucion completa.
	 */
	private static boolean aceptable(int i, int j, int iNext, int jNext, Solucion solucion, int dMin, int[][] matrizEdicion, String s1, String s2){
		boolean resultado = solucion.getDistancia() <= dMin;
		if (i == iNext){
			if (matrizEdicion[i][j]<=matrizEdicion[i][jNext]) resultado = false;
			if (Math.abs(matrizEdicion[i][j] - matrizEdicion[iNext][jNext]) != 1) resultado = false;
		} else if (j == jNext){
			if (matrizEdicion[i][j]<=matrizEdicion[iNext][j]) resultado = false;
			if (Math.abs(matrizEdicion[i][j] - matrizEdicion[iNext][jNext]) != 1) resultado = false;
		} else {
			if (matrizEdicion[i][j]<matrizEdicion[iNext][jNext]) resultado = false;
			if (Math.abs(matrizEdicion[i][j] - matrizEdicion[iNext][jNext]) > 1) resultado = false;
			if (matrizEdicion[i][j]==matrizEdicion[iNext][jNext] && s1.charAt(iNext) != s2.charAt(jNext)) resultado = false;
		}
		return resultado;
	}
	
	/*
	 * Encapsula los calculos de la matriz de edición, con el objetivo de
	 * poder ser reutilizado.
	 */
	private static int[][] calculaMatrizEdicion(String s1, String s2){
		int edicion,adjI,adjA,adjD;
		int l1 = s1.length() + 1;
		int l2 = s2.length() + 1;
		int[][] matrizEdicion = new int[l1][l2];
		for(int i=0;i<matrizEdicion.length;i++){
			for(int j=0;j<matrizEdicion[i].length;j++){
				if(Math.min(i, j) == 0)
					matrizEdicion[i][j] = Math.max(i,j);
				else {
					edicion = s1.charAt(i-1) == s2.charAt(j-1) ? 0 : 1;
					adjI = matrizEdicion[i-1][j] + 1;
					adjA = matrizEdicion[i][j-1] + 1; 
					adjD = matrizEdicion[i-1][j-1] + edicion;
					
					matrizEdicion[i][j] = Math.min(Math.min(adjI, adjA), adjD);
				}
			}
		}
		return matrizEdicion;
	}
	
	/*
	 * Imprime por pantalla la matriz de edicion correspondiente al caso que se
	 * especifique.
	 */
	private static void printMatrizEdicion(String s1, String s2, int[][] mEd){
		// TODO: funciona para los casos en los que max{|s1|,|s2|} < 10, usar tabs
		s1 = "-" + s1;
		s2 = "-" + s2;
		System.out.print(" ");
		for(int a=0;a<s2.length();a++){
			System.out.print(" " + s2.charAt(a));
		}
		System.out.print("\n");
		for(int i=0;i<mEd.length;i++){
			System.out.print(s1.charAt(i));
			for(int j=0;j<mEd[i].length;j++){
				System.out.print(" " + mEd[i][j]);
			}
			System.out.print("\n");
		}
	}	
	
	/**
	 * Lee un fichero de texto, obteniendo como cadena todas las lineas
	 * existentes, despreciando las vacias.
	 * 
	 * @param ruta - ruta del fichero a leer.
	 * @return Array de String con todas las cadenas no vacias leidas
	 */
	public static String[] leeFichero(String ruta){
		ArrayList<String> listaCadenas = new ArrayList<String>();
		
		BufferedReader reader = null;
		try {
			reader = new BufferedReader(new InputStreamReader(new FileInputStream(ruta)));
		} catch (FileNotFoundException e) {
			System.err.println("Error al abrir el fichero.");
		}
		String s;
		try {
			while ((s = reader.readLine()) != null){
				if (!s.equals("")) // NO HAY QUE DESPRECIAR LAS LINEAS VACIAS: Podemos querer conocer la distancia entre la cadena vacia y otra
					listaCadenas.add(s);
			}
			reader.close();
		} catch (IOException e) {
			System.err.println("Error al leer en fichero");
		}
		String[] cadenas = new String[listaCadenas.size()];
		cadenas = listaCadenas.toArray(cadenas);
		return cadenas;
	}
	
	/**
	 * Añade a un fichero existente (o crea uno si no existe) la cadena
	 * especificada como argumento.
	 * 
	 * @param ruta - ruta al fichero en el que escribir.
	 * @param cadena - La cadena a añadir al fichero.
	 */
	public static void imprimeFichero(String ruta, String cadena){
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(new BufferedWriter(new FileWriter(ruta, true)));
		} catch (IOException e) {
			System.err.println("Error en la escritura.");
		}
		writer.println(cadena);
		writer.flush();
		writer.close();
	}
	
	/**
	 * Inicializa un fichero, elimiando los posibles contenidos que pudiera 
	 * contener.
	 * 
	 * @param ruta - fichero a inicializar.
	 */
	public static void vaciaFichero(String ruta){
		PrintWriter writer = null;
		try {
			writer = new PrintWriter(new BufferedWriter(new FileWriter(ruta, false)));
		} catch (IOException e) {
			System.err.println("Error al inicializar el fichero.");
		}
		writer.flush();
		writer.close();
	}
}
