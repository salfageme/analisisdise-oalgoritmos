package uva.ada.samalfa_danbarb.quicksort;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Random;

/**
 * Efectua una serie de calculos, generando un archivo csv con información de
 * las sucesivas ejecuciones del algoritmo quicksort y los algoritmos de inserción,
 * burbuja y seleccion, con el objetivo de poder estudiar estos datos para encontrar
 * el umbral a partir del cual el algoritmo de ordenacion se beneficia de cambiar a uno
 * de los algoritmos basicos de ordenacion.
 * 
 * @author Samuel Alfageme
 * @author Daniel Barba
 *
 */
public class QuicksortAnalyzer {
	private static final int ITERACIONES = 25;
	private static final int UMBRAL = 20;

	public static void main(String[] args){
		analizar(3, 100, "small.csv");
		analizar(100, 1000, "medium.csv");
		analizar(1000, 1000000, "large.csv");
	}
	
	/**
	 * Realiza las correspondientes llamadas a los metodos escritos para los algoritmos
	 * de ordenacion insercion, seleccion, burbuja y quicksort, un numero determinado
	 * de veces para cada tamaño y para un intervalo de tamaños de vector, 
	 * almacenando la media de dichas iteraciones en un fichero especificado.
	 * 
	 * @param min - Tamaño inicial de vector
	 * @param max - Tamaño final de vector
	 * @param filename - Ruta de fichero donde almacenar los datos
	 */
	public static void analizar(int min, int max, String filename){
		int i,j,k;
		int n = 1;
		int[] v;
		long[] datos;
		double comp, asig, time;
		boolean warmUp = true;
		
		if (min >= 100)		n = 10;
		if (min >= 1000)	n = 1000;
		
		headerToCSV(filename);
		for (i = min; i <= max; i+=n){
			long[] medias = new long[16];
			medias[0] = i;
			for (j = 0; j < 5; j++){
				comp = asig = time = 0;
				for (k = 0; k < ITERACIONES; k++){
					v = vectorGenerator(i);
					switch (j){
					case 0:
						datos = insercion(v);
						break;
					case 1:
						datos = seleccion(v);
						break;
					case 2:
						datos = burbuja(v);
						break;
					case 3:
						datos = quicksort(v);
						break;
					case 4:
					default:
						datos = quicksortModificado(v);
						break;
					}
					comp += datos[1]/(double) ITERACIONES;
					asig += datos[2]/(double) ITERACIONES;
					time += datos[3]/(double) ITERACIONES;
				}
				medias[1+j*3] = Math.round(comp);
				medias[2+j*3] = Math.round(asig);
				medias[3+j*3] = Math.round(time);
			}
			/*
			 * Se ha descubierto que, al ser Java un lenguaje interpretado, durante el comienzo del bucle
			 * realiza una serie de operaciones que siempre afectan a las mediciones de tiempo de las
			 * primeras iteraciones. Para solventar este inconveniente, hacemos unas primeras iteraciones
			 * de "calentamiento" que descartamos antes de empezar a aceptar datos.
			 */
			if (warmUp) {
				if (i>=Math.ceil(50000.0/min) || i>=max/2){
					warmUp = false;
					i=min-n;
				}
			} else
				exportToCSV(filename, medias);
			if (min>=1000 && i>=10*n)
				n*=10;
		}
	}
	
	/**
	 * Genera un vector de enteros generados pseudoaleatoriamente.
	 * @param t: Tamaño del vector a generar
	 * @return el vector del tamaño especificado inicializado.
	 */
	public static int[] vectorGenerator(int t){
		Random random = new Random();
		int[] v = new int[t];
		for (int i = 0; i < v.length; i++)	v[i] = random.nextInt(t);
		return v;
	}
	
	/**
	 * Ordena un vector mediante el algoritmo de inserción y analiza el numero de
	 * operaciones de comparacion y asignaciones en el que se usa el vector.
	 * @param v: El vector a ordenar
	 * @return Una tupla con los datos [#elementos, #comparaciones, #asignaciones, nanosegundos]
	 */
	public static long[] insercion(int[] v){
		long t0 = System.nanoTime();
		long comp = 0;
		long asig = 0;
		int i,j,key;
		for(j=1; j<v.length; j++){
			key = v[j];
			asig++;
			// Insertar vector[j] en la secuencia ordenada vector[1..j-1]
			i = j-1;
			while((i>=0) && (v[i]>key)){
				comp++;
				v[i+1] = v[i];
				asig++;
				i = i-1;
			}
			if(i>=0) comp++; //la ultima comparacion del while
			v[i+1] = key;
			asig++;
		}
		long tiempo = System.nanoTime() - t0;
		long[] t = {v.length, comp, asig, tiempo};
		return t;
	}
	
	/**
	 * Ordena un vector mediante el algoritmo de seleccion y analiza el numero de
	 * operaciones de comparacion y asignaciones en el que se usa el vector.
	 * @param v: El vector a ordenar
	 * @return Una tupla con los datos [#elementos, #comparaciones, #asignaciones, nanosegundos]
	 */
	public static long[] seleccion(int[] v){
		long t0 = System.nanoTime();
		long comp = 0;
		long asig = 0;
		
		int jMin;
		for (int i=0; i<v.length-1; i++){
			jMin = i;
			for (int j=i+1; j<v.length; j++){
				if (v[j] < v[jMin]){
					jMin = j;
				}
				comp++;
			}
			int aux = v[i];
			v[i] = v[jMin];
			v[jMin] = aux;
			asig+=3;
		}
		
		long tiempo = System.nanoTime() - t0;
		long[] t = {v.length, comp, asig, tiempo};
		return t;
	}
	
	/**
	 * Ordena un vector mediante el algoritmo de burbuja y analiza el numero de
	 * operaciones de comparacion y asignaciones en el que se usa el vector. La version
	 * del algoritmo es la encontrada en las transparencias del Tema 3 de la asignatura.
	 * @param v: El vector a ordenar
	 * @return Una tupla con los datos [#elementos, #comparaciones, #asignaciones, nanosegundos]
	 */
	public static long[] burbuja(int[] v){
		long t0 = System.nanoTime();
		long comp = 0;
		long asig = 0;
		
		for (int i=0; i<v.length; i++){
			for (int j=(v.length-1); j>i; j--){
				if (v[j-1] > v[j]){
					int aux = v[j-1];
					v[j-1] = v[j];
					v[j] = aux;
					asig+=3;
				}
				comp++;
			}
		}
		
		long tiempo = System.nanoTime() - t0;
		long[] t = {v.length, comp, asig, tiempo};
		return t;
	}
	
	/**
	 * Método auxiliar para simplificar la primera llamada al procedimiento general de ordenación rápida
	 * 
	 * @param v - el array de números enteros a ordenar aplicando el algoritmo
	 */
	public static long[] quicksort(int [] v){
		long[] subDatos = new long[2];
		long t0 = System.nanoTime();
		subDatos = quicksort(v,0,v.length-1);
		long tiempo = System.nanoTime() - t0;
		long[] t = {v.length, subDatos[0], subDatos[1], tiempo};
		return t;
	}
	
	/**
	 * Método general de ordenación rápida (Quicksort)	
	 * 
	 * @param vector - el array de números enteros que se va a ordenar
	 * @param ini - la posición de partida absoluta de una secuencia desordenada
	 * @param fin - la posición final de una secuencia desordenada
	 */
	public static long[] quicksort(int vector[], int ini, int fin) {
		long[] der = {0,0}, izq = {0,0} , total = new long[2];
		long[] part = partitionLomuto(vector,ini,fin);
		int j = (int) part[2];
		if(ini < j-1)
			der = quicksort(vector,ini,j-1); 
		if(j+1 < fin)
			izq = quicksort(vector,j+1,fin);
		for(int i=0;i<total.length;i++){
			total[i] = der[i]+izq[i]+part[i]; 
		}
		return total;
	}
	
	/**
	 * Método auxiliar para simplificar la primera llamada al procedimiento general de ordenación rápida
	 * modificado para incorporar la ordenacion por insercion en vectores de tamaño por debajo de un umbral
	 * 
	 * @param v - el array de números enteros a ordenar aplicando el algoritmo
	 */
	public static long[] quicksortModificado(int [] v){
		long[] subDatos = new long[2];
		long t0 = System.nanoTime();
		subDatos = quicksortModificado(v,0,v.length-1);
		long tiempo = System.nanoTime() - t0;
		long[] t = {v.length, subDatos[0], subDatos[1], tiempo};
		return t;
	}
	
	/**
	 * Método general de ordenación rápida (Quicksort) modificado con Insercion.	
	 * 
	 * @param vector - el array de números enteros que se va a ordenar
	 * @param ini - la posición de partida absoluta de una secuencia desordenada
	 * @param fin - la posición final de una secuencia desordenada
	 */
	public static long[] quicksortModificado(int vector[], int ini, int fin) {
		long[] total = new long[2];
		if (fin-ini>=UMBRAL){
			long[] der = {0,0}, izq = {0,0};
			long[] part = partitionLomuto(vector,ini,fin);
			int j = (int) part[2];
			if(ini < j-1)
				der = quicksort(vector,ini,j-1); 
			if(j+1 < fin)
				izq = quicksort(vector,j+1,fin);
			for(int i=0;i<total.length;i++){
				total[i] = der[i]+izq[i]+part[i]; 
			}
		} else {
			long comp = 0;
			long asig = 0;
			int i,j,key;
			for(j=ini; j<=fin; j++){
				key = vector[j];
				asig++;
				// Insertar vector[j] en la secuencia ordenada vector[1..j-1]
				i = j-1;
				while((i>=ini) && (vector[i]>key)){
					comp++;
					vector[i+1] = vector[i];
					asig++;
					i = i-1;
				}
				if(i>=ini) comp++; //la ultima comparacion del while
				vector[i+1] = key;
				asig++;
			}
			total[0] = comp;
			total[1] = asig;
		}
		return total;
	}
	
	/**
	 * Algoritmo de particionado de Hoare (transcrito del pseudocódigo de Introduction to Algorithms - H.Cormen : p185)
	 * el pivote puede ser cualquier elemento del array, pero este algorimo necesita que se encuentre en la primera
	 * posición, por lo que se realiza una operación de intercambio al comienzo
	 * 
	 * @param vector - el array que contiene la secuencia de números enteros a ordenar
	 * @param ini - el índice de inicio de la secuencia, relativo a vector
	 * @param fin - el índice de la posición final de la secuencia, relativo a vertor
	 * @return j - la posición final absoluta del pivote en vector
	 */
	
	public static long[] partitionHoare(int [] vector, int ini, int fin) {
		long comp = 0;
		long asig = 0;
		int i = ini, j = fin;
		int aux;
		
		int pivote = vector[ini];
		asig++;
		 
		while(i<j){
			while(vector[i]<=pivote && i<j){
				comp++;
				i++; 
			}
			comp++; //La ultima comparacion del while
			while(vector[j]>pivote){
				comp++;
				j--;         	
			}
			comp++; //La ultima comparacion del while
			if (i<j) {
				aux = vector[i];                 	
			    vector[i] = vector[j];
			    vector[j] = aux;
			    asig+=3;
			}
		}
		vector[ini] = vector[j];
	   	vector[j] = pivote;
	   	asig+=2;
		long[] datos = {comp,asig,j};
		return datos;
	}
	
	/**
	 * Algoritmo de particionado de Lomuto (transcrito del pseudocódigo de Introduction to Algorithms - H.Cormen : p171)
	 * @param vector - el array que contiene la secuencia de números enteros a ordenar
	 * @param ini - el índice de inicio de la secuencia, relativo a vector
	 * @param fin - el índice de la posición final de la secuencia, relativo a vertor
	 * @return j - la posición final absoluta del pivote en vector
	 */
	
	public static long[] partitionLomuto(int [] vector, int ini, int fin) {
		long comp = 0;
		long asig = 0;
		int i = ini-1, j;
		int aux;

		int pivote = vector[fin];
		asig++;
		
		for(j=ini;j<fin;j++){
			if(vector[j]<=pivote){
				i++;
				aux = vector[i]; 
				vector[i] = vector[j];
				vector[j] = aux;
				asig+=3;
 			}
			comp++;
		}
		aux = vector[i+1];
		vector[i+1] = pivote;
		vector[fin] = aux;
		asig+=3;
		long[] datos = {comp,asig,i+1};
		return datos;
	}
	
	/**
	 * Añade a un fichero (o lo crea si no existe) la tupla de datos especificados.
	 * 
	 * @param fileName: Nombre del fichero
	 * @param datos: Tupla con los datos [#elementos, #comparaciones, #asignaciones, nanosegundos, ...]
	 */
	public static void exportToCSV(String fileName, long[] datos){
		try {
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(fileName, true)));
			
			for(int i = 0; i<datos.length; i++){
				writer.print(datos[i]);
				if (i!=datos.length-1)	writer.print(",");
			}
			writer.println();
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Crea un fichero al que añade la cabecera del csv.
	 * 
	 * @param fileName: Nombre del fichero
	 */
	public static void headerToCSV(String fileName){
		try {
			PrintWriter writer = new PrintWriter(new BufferedWriter(new FileWriter(fileName, false)));
			writer.print("Elementos,Comp Insercion,Asig Insercion,Tiempo Insercion,"
					+ "Comp Seleccion,Asig Seleccion,Tiempo Seleccion,"
					+ "Comp Burbuja,Asig Burbja,Tiempo Burbuja,"
					+ "Comp Quicksort,Asig Quicksort,Tiempo Quicksort,"
					+ "Comp QuicksortMod,Asig QuicksortMod,Tiempo QuicksortMod");
			writer.println();
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
