package uva.ada.samalfa_danbarb.palabrasEncadenadas;

import java.util.ArrayList;
import java.util.Stack;

/**
 * Práctica 3 : Análisis y Diseño de Algoritmos - Curso 2014/2015
 * 
 * Clase que representa un grafo no dirigido simple G = (V,A) cuyo conjunto de
 * vértices V se representa mediante un vector de String de la misma longitud.
 * El conjunto de aristas A se almacenará en una lista de adyacencia.
 * 
 * @author Samuel Alfageme Sainz (samalfa)
 * @author Daniel Barba Gutiérrez (danbarb)
 */
public class GrafoPalabras {
	
	private static final int INF = Integer.MAX_VALUE;
	
	private String[] diccionario;
	private ArrayList<ArrayList<Integer>> listaAdyacencia;
	
	/**
	 * Contrucción de un Grafo, asociando el conjunto de palabras pasadas como 
	 * argumento al conjunto de vertices V, y determinando la existencia de 
	 * aristas entre estos en base a las reglas establecidas para el juego.
	 * 
	 * @param diccionario - el vector de String que representa el conjunto de 
	 * vertices del grafo.
	 */
	public GrafoPalabras(String[] diccionario){
		this.diccionario = diccionario;
		listaAdyacencia = new ArrayList<ArrayList<Integer>>(diccionario.length);
		poblarLAdyacencia();
	}
	
	/**
	 * Método para la búsqueda de una palabra dentro del conjunto de vertices
	 * 
	 * @param palabra - la palabra a buscar en el grafo
	 * @return el índice asociado a dicha palabra dentro de conjunto ordenado
	 * de vértices, o el valor -1 en caso de que no esté presente.
	 */
	public int indicePalabra(String palabra){
		for(int i=0 ; i< diccionario.length ; i++){
			if(diccionario[i].equals(palabra)) return i;
		}
		return -1;
	}
	
	/**
	 * Método para la obtención de la palabra asociada al índice de uno de los 
	 * vértices del grafo.
	 * 
	 * @param indice - La posición que ocupa la palabra dentro del conjunto 
	 * ordenado de vértices.
	 * @return la palabra en dicho índice.
	 */
	public String palabraEnIndice(int indice){
		return diccionario[indice];
	}
	
	
	/**
	 * Función que determina la existencia de una arista en el grafo: si la 
	 * diferencia de caracteres entre 2 palabras(vertices) es 1, existe arista 
	 * entre ellas.
	 * 
	 * @Precondition: las cadenas s1 y s2 tienen la misma longitud
	 * 
	 * @param s1 - Cadena 1
	 * @param s2 - Cadena 2
	 * @return - true en el caso en el que entre los vértices sólo exista un
	 * caracter de diferencia; false en caso contrario 
	 */
	public static boolean distanciaIgualUno(String s1, String s2){
		int charDist = 0;
		for(int i=0; i<s1.length(); i++){
			if(s1.charAt(i) != s2.charAt(i)) charDist++;
		}
		return charDist == 1; 
	}
	
	/**
	 * Recorre el subdiccionario, añadiendo, para cada indice del diccionario,
	 * (equivalente al de la lista de adyacencia) el índice de los nodos que se
	 * encuentran a una distancia 1 de éste.
	 */
	public void poblarLAdyacencia(){
		for(int l=0; l<diccionario.length; l++){
			listaAdyacencia.add(new ArrayList<Integer>());
		}
		for(int i=0; i<diccionario.length-1; i++){
			for(int j=i+1; j<diccionario.length; j++){
				if(distanciaIgualUno(diccionario[i],diccionario[j])){
					listaAdyacencia.get(i).add(j);
					listaAdyacencia.get(j).add(i);
				}
			}
		}
	}
	
	/**
	 * Implementación del Algoritmo de Dikjstra para el cálculo de un camino 
	 * mínimo entre 2 vertices s1 y s2 sobre el objeto GrafoPalabras
	 * 
	 * @param s1 - La palabra de partida para el juego
	 * @param s2 - La palabra que se debe alcanzar aplicando las reglas.
	 * @return La lista de palabras asociada a la secuencia de pasos encontrada 
	 * como solución al juego  
	 * @throws Exception en caso de que se produzca algún error en la obtencion
	 * de la secuencia de sustituciones.
	 */
	public String[] getCSolucion(String s1, String s2) throws Exception{
		
		int i1 = indicePalabra(s1.toUpperCase());
		int i2 = indicePalabra(s2.toUpperCase());
		
		if(i1==-1 || i2==-1) 
			throw new Exception("Alguna de las entradas no se encuentra en el"
					+ " Diccionario.");
		
		int n = diccionario.length;
		
		MinHeap vectorD = new MinHeap(n);
		int[] vectorP = new int[n];
		boolean[] vectorS = new boolean[n];
		
		for(int i=0 ; i<n ; i++){
			vectorP[i] = -1;
			vectorS[i] = true;
		}
		
		vectorP[i1] = i1;
		vectorD.inicializarDesde(i1);
		
		int[] u, k;
		
		for(int i=0 ; i<n ; i++){

			if(vectorD.heap[0][1] == INF)
				throw new Exception("No es posible generar una secuencia de "
						+ "sustituciones entre " + s1 + " y " + s2);
			
			u = vectorD.extraerMinimo();
			vectorS[u[0]] = false;
			
			if(u[0] == i2)
				break;
			
			for(int m : listaAdyacencia.get(u[0])){
				if(vectorS[m]){
					k = vectorD.getDist(m);
					if(k[1] > u[1] + 1){
						vectorD.heap[k[0]][1] = u[1] + 1;
						vectorD.flotar(k[0]);
						vectorP[m] = u[0];
					}
				}
			}
		}
		
		int[] secuenciaIndices = construirCSolucion(vectorP,i2,i1);
		
		String[] pasos = new String[secuenciaIndices.length];
		for(int i=0; i<secuenciaIndices.length;i++)
			pasos[i] = palabraEnIndice(secuenciaIndices[i]);
		
		return pasos;
	}
	
	/**
	 * Método para la construcción de la secuencia solución que representa
	 * el camino obtenido mediante el algoritmo de Dikstra a partir del vector 
	 * P.
	 * 
	 * @param indices - una lista de predecesores que representa el vector P 
	 * obtenido tras aplicar el algoritmo de Dikjstra
	 * @param desde
	 * @param hasta
	 * @return La lista de indices asociadas a la secuencia encontrada
	 */
	public static int[] construirCSolucion(int[] indices, int desde, int hasta){
		Stack<Integer> secuencia = new Stack<Integer>();
		int i = desde;
		int iP =  indices[i];
		while(i!=hasta){
			secuencia.push(i);
			i = iP;
			iP = indices[i];
		}
		secuencia.push(iP);
		int[] sec = new int[secuencia.size()];
		for(int j=0; j<sec.length; j++) sec[j] = secuencia.pop();
		return sec;
	}
	
	/**
	 * Clase interna para la implementación del vector de distancias D 
	 * utilizado por el algoritmo de Dikstra mendiante un montículo binario de 
	 * mínimos, para la eficiente extracción del valor mínimo. El montículo 
	 * ordenará pares (i, distancia) donde i será el índice único del nodo y 
	 * distancia la menor distancia encontrada desde el nodo de partida hasta 
	 * el nodo i. 
	 * 
	 * p.ej: el nodo 10 con peso 3 aparecerá en el montículo como (10,3)
	 */
	public static class MinHeap{
		
		private int[][] heap;
		private int size;
		
		/**
		 * Creación de un nuevo montículo vacío de tamaño size
		 * 
		 * @param size - el tamaño de creación del montículo
		 */
		public MinHeap(int size){
			this.size = size;
		}
		
		/**
		 * Operación de inicialización del montículo, todos los nodos, excepto
		 * aquel que se especifique como inicial (que se pondrá a valor 0 y 
		 * pasará a ocupar la cima del montículo) se inicializan a infinito.
		 * 
		 * @param i - el nodo que se pondrá a 0 y ocupará la posicion 0 del 
		 * montículo.
		 */
		public void inicializarDesde(int i){
			heap = new int[size][2];
			for(int j=0; j<size ; j++){
				heap[j] = new int[]{j,INF};
			}
			heap[i] = new int[]{i,0};
			swapElements(0,i);
		}
		
		/**
		 * Se genera una tupla (posicion_interna, distancia) referida a los 
		 * valores del nodo que ocupa el indice i dentro del montículo.
		 * posicion_interna: posicion que ocupa el nodo i dentro del monticulo
		 * 
		 * @param i - el indice respecto al subdiccionario que se busca en el 
		 * monticulo
		 * @return la tupla correspondiente asociada a dicho índice. En caso de 
		 * no encontrarse se devuelve la tupla (-1,∞)
		 */
		public int[] getDist(int i){
			for(int j=0 ; j<size ; j++){
				if(heap[j][0] == i) return new int[]{j,heap[j][1]};
			}
			return new int[]{-1,INF};
		}
		
		/**
		 * Realiza el intercambio de 2 elementos de indices i1, i2 en el 
		 * monticulo
		 * 
		 * @param i1 - el indice del primer elemento intercambiado
		 * @param i2 - el indice del segundo elemento intercambiado
		 */
		private void swapElements(int i1, int i2){
			int[] auxiliar = heap[i1];
			heap[i1] = heap[i2];
			heap[i2] = auxiliar;
		}
		
		/**
		 * Operación de reestructuración del montículo que hace que el nodo i
		 * pase a ocupar el lugar que le corresponde, descendiendo posiciones 
		 * hasta que cumpla la condicion de monticulo respecto a todos los 
		 * nodos descendientes
		 * 
		 * @param i - el índice del nodo del montículo a hundir
		 */
		public void hundir(int i){
			int p = i;
			int h = i*2+1;
			if(h+1<size && heap[h][1]>heap[h+1][1]) h++;
			while(h<size && heap[p][1] > heap[h][1]){
				swapElements(p,h);
				p=h;
				h = 2*p+1;
				if(h+1<size && heap[h][1]>heap[h+1][1]) h++;
			}
		}
		
		/**
		 * Operación de reestructuración del montículo que hace que el nodo i
		 * pase a ocupar el lugar que le corresponde, ascendiendo posiciones 
		 * hasta que cumpla la condicion de monticulo respecto a todos sus 
		 * nodos predecesores
		 * 
		 * @param i - el índice del nodo del montículo a flotar
		 */
		public void flotar(int i){
			int h = i;
			int p = (i-1)/2;
			while(p>=0 && heap[h][1] < heap[p][1]){
				swapElements(h,p);
				h = p;
				p = (h-1)/2;
			}
		}

		/**
		 * Método de extracción de la tupla con peso mínimo del montículo.
		 * 
		 * @return la tupla (indice,peso) del nodo con menor peso del monticulo
		 */
		public int[] extraerMinimo(){
			int[] minimo = heap[0];
			heap[0] = heap[size-1];
			size--;
			hundir(0);
			return minimo;
		}
	}
}
